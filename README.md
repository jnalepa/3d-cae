**Unsupervised Segmentation of Hyperspectral Images Using 3D Convolutional Autoencoders**

Jakub Nalepa, Member, IEEE, Michal Myller, Yasuteru Imai, Ken-ichi Honda, Tomomi Takeda, Marek Antoniak

(Paper submitted to IEEE GRSL)

This repository contains: 

* Supplementary Material containing a visualization of the Mullewa set, alongside example segmentations of this HSI obtained using selected variants of the investigated algorithms.
* All visualizations generated using the investigated methods (with and without HSI reduction).